# Singularity_Installation

https://sylabs.io/guides/3.5/user-guide/quick_start.html

```
sudo apt-get update && sudo apt-get install -y \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup
```

Install Go
Singularity v3 and above is written primarily in Go, so you will need Go installed to compile it from source.

```
# Replace the values as needed
export VERSION=1.13.7 OS=linux ARCH=amd64
# Downloads the required Go package
wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz
# Extracts the archive
sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz
# Deletes the tar file
rm go$VERSION.$OS-$ARCH.tar.gz
echo 'export PATH=/usr/local/go/bin:$PATH' >> ~/.bashrc && source ~/.bashrc
```
Download Singularity from a release


```
 # adjust this as necessary
export VERSION=3.5.2
wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz
tar -xzf singularity-${VERSION}.tar.gz
cd singularity
#to avoid error with libseccomp
PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig ./mconfig
PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig make -C builddir
sudo PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig make -C builddir install
singularity help
```
